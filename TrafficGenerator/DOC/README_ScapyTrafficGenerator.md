#  README  #

this is the readme file on use of ScapyTrafficGenerator and ScapyTrafficController

ScapyTrafficGenerator is a versitile packet generator that will take arguments to define ips, ports, etc. will be located in /bin/usr/ScapyTrafficGenerator
ScapyTrafficController with generate packets based on is its own self contained config file. will be located in /bin/usr/ScapyTrafficController


## Installation ##

Unzip tar file on ts-test/Tools/TrafficGenerator.tar.gz

    tar xvf TrafficGenerator.tar.gz

run the setup

    cd TrafficGenerator
    ./setup.sh

Edit Controller (optional)

    modify settings to your liking in /bin/usr/ScapyTrafficController

#ScapyTrafficGenerator#

## Basic Uses ##

-F File

-i interface to run

-s src ip

-S src PORT

-d dst ip

-D dst port

-m scr mac

-M dst mac

-H content length

-X Method

-E Encoding

##Basic Examples ##

Run binary HTTP Get of malicious file from source 1.1.1.1 to dest 2.2.2.2

   ScapyTrafficGenerator -X http -r '-X 1 -F /files/Malicious_DOC.doc -i eth0 -s 1.1.1.1 -S 33333 -d 2.2.2.2 -D 80 -m 12:31:25:15:a1:55 -M 12:31:25:15:a1:2b'

Any value not specified in arguments will be randomized.
Run HTTP Get with chunked encoding length of 200 from random source and destination

    ScapyTrafficGenerator -X http -r '-X 1 -E 2 -H 200 -F /files/Malicious_DOC.doc -i eth0'

Run HTTP Get with gzip compression form source 1.1.1.1 to random dest

    ScapyTrafficGenerator -X http -r '-X 1 -E 3 -F /files/Malicious_DOC.doc -i eth0 -s 1.1.1.1'

Run HTTP Get with gzip compression and chuncked encoding with length of 300

     ScapyTrafficGenerator -X http -r '-X 1 -E 4 -H 200 -F /files/Malicious_DOC.doc -i eth0
    
Run binary HTTP put from random source and destination 2.2.2.2

     ScapyTrafficGenerator -X http -r '-X 2 -F /files/Malicious_DOC.doc -i eth0 -d 2.2.2.2'

Run multiform HTTP post from source 1.1.1.1 to with port 10000 to random dest

    ScapyTrafficGenerator -X http -r '-X 3 -E 5 -p -F /files/Malicious_DOC.doc -i eth0 -s 1.1.1.1 -S 10000'

Run binary FTP transfer with random src and dest ips, command source port 50 and comand dest port 51 data dest port 33333 and data source port 55555

    #get
    ScapyTrafficGenerator -X ftp -r '-X 4 -F /files/Malicious_DOC.doc -i eth0 -c 50 -C 51 -S 55555 -D 33333'
    #put with active ftp
    ScapyTrafficGenerator -X ftp -r '-X 5 -F /files/Malicious_DOC.doc -i eth0 -c 50 -C 51 -S 55555 -D 33333 -a'
    #append 
    ScapyTrafficGenerator -X ftp -r '-X 6 -F /files/Malicious_DOC.doc -i eth0 -c 50 -C 51 -S 55555 -D 33333'
    

Run FTP Get Segmentation with 4 segments to randomized src and dest

    ScapyTrafficGenerator -X ftp -r '-X 4 -F /files/Malicious_DOC.doc -i eth0 -z 4'

Run DHCP discover to get source ip 1.1.1.1 from dhcp server 2.2.2.2

    ScapyTrafficGenerator -X dhcp -r '-i eth0 -s 1.1.1.1 -d 2.2.2.2

Run DHCP discover to get source ip 1.1.1.1 from dhcp server 2.2.2.2 and give route 2.2.2.1 with dnsserver 2.2.2.3 dns tts.local and subnet 255.255.255.0 

    ScapyTrafficGenerator -X dhcp -r '-i eth0 -s $SIP -d 1.1.1.1 -m 12:31:25:15:a1:55 -M 12:31:25:15:a1:2b -N 2.2.2.3 -u 255.255.255.0 -r 2.2.2.1 -n tts.local

Run DHCP inform request with dns name tts.local

    ScapyTrafficGenerator -X dhcp -r '-i eth0 -s 1.1.1.1 -d 2.2.2.2 -n tts.local -f

Run DNS lookup of test.net and return address 1.2.3.4

    ScapyTrafficGenerator -X dhcp -r '-i eth0 -s 1.1.1.1 -d 2.2.2.2 -m 12:31:25:15:a1:55 -M 12:31:25:15:a1:2b -N test.net:1.2.3.4



#ScapyPacketController#

All tests use built in config file. You can edit this file in /usr/bin/ScapyTrafficController.  all variable fields are predefinded so you dont need a run command like -r in ScapyPcapGenerator.

Run basic tests


    ScapyTrafficController -X http_get_bin
    ScapyTrafficController -X http_get_chunked
    ScapyTrafficController -X http_get_gzip
    ScapyTrafficController -X http_put_bin
    ScapyTrafficController -X http_put_multipart
    ScapyTrafficController -X http_post_bin
    ScapyTrafficController -X http_post_multipart
    ScapyTrafficController -X ftp_get
    ScapyTrafficController -X ftp_put
    ScapyTrafficController -X ftp_append
    ScapyTrafficController -X ftp_get_segmented
    ScapyTrafficController -X dns
    ScapyTrafficController -X dhcp


mix things up,  if you want to run a coupld of tests. to run dhcp then dns then http then ftp then dns then dhcp.

    ScapyTrafficController -X dhcp -X dns -X http -X ftp -X dns -X dhcp

    

    
