from scapy.all import *
from subprocess import Popen, PIPE
import os
import sys


## only works with TCP/udp packets
def Scapy_Pcap_to_JSON(ScapyPcapFormat,
                       Name = 'packets'):
    jsonpackets = {}
    jsonpackets['packets'] = Name
                       
    for packet in range(len(ScapyPcapFormat)):
        PACKET = {}
        if ScapyPcapFormat[packet].haslayer(Ether):
            PACKET['ether'] = ScapyPcapFormat[packet][Ether].fields
        if ScapyPcapFormat[packet].haslayer(IP):
            PACKET['ip'] = ScapyPcapFormat[packet][IP].fields
        if ScapyPcapFormat[packet].haslayer(UDP):
            PACKET['udp'] = ScapyPcapFormat[packet][UDP].fields
        if ScapyPcapFormat[packet].haslayer(TCP):
            PACKET['tcp'] = ScapyPcapFormat[packet][TCP].fields
        if ScapyPcapFormat[packet].haslayer(Raw):
            PACKET['raw'] = ScapyPcapFormat[packet][Raw].fields
        jsonpackets['%i' %packet] = PACKET
    return jsonpackets

if __name__ == '__main__':
    r = rdpcap('../../../FTPPCAPS/TESTCAPS/FTP_GET.pcap')
    pcapjson = Scapy_Pcap_to_JSON(r)
    for pcap in pcapjson.keys():
        print pcapjson[pcap]
#copypcapfile('multipost.pcap', 'badmulti.txt')  
    
