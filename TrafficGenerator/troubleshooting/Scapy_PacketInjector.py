#! /usr/bin/env python
from scapy.all import *
import os
import random
import sys, getopt
from time import sleep, time, gmtime
from subprocess import Popen, PIPE
from xml.dom.minidom import parse
import threading
from time import sleep, asctime
import re


                        
class Scapy_PacketInjector():
    def __init__(self,
                 PCAPPATH,
                 ORIGINALSRC,
                 ORIGINALDST):
        self.pcapfile = rdpcap(PCAPPATH)
        self.ORIGINALSRC = ORIGINALSRC
        self.ORIGINALDST = ORIGINALDST
        self.SMAC = None
        self.DMAC = None
        self.IterTCPPackets()
        self.INT = None
    def SetINTF (self,
                 INTF):
        self.INT = INTF
        
    def IterTCPPackets(self):
        self.IterPackets={#src_dstport:[packetnumber,
                                       #fromSrc,
                                        #ack,
                                       #seq,
                                        #rawlen]
                            
                            }
        self.IterPackets['NA'] = []
        self.PacketList=[#p,
                        #flow,
                        ]
        #print 'iterate packets'
        #print 'number of packets %i' %len(self.pcapfile)
        for p in range(len(self.pcapfile)):
            #print 'packet number', p
            if self.pcapfile[p].haslayer(TCP) == 1:
                if self.pcapfile[p].haslayer(Raw):
                    rawlen = len(self.pcapfile[p][Raw].load)
                else:
                    rawlen = 0
                cont = 0
                if (self.pcapfile[p][IP].src == self.ORIGINALSRC ) and ( self.pcapfile[p][IP].dst == self.ORIGINALDST ):
                    #print 'match sip_dip'
                    flow = '%s_%s' %(str(self.pcapfile[p][TCP].sport),str(self.pcapfile[p][TCP].dport))
                    #print 'flow %s' %flow
                    fromSrc = True
                    ack= self.pcapfile[p][TCP].ack
                    seq= self.pcapfile[p][TCP].seq
                    smac= self.pcapfile[p][Ether].src
                    dmac= self.pcapfile[p][Ether].dst
                    if self.SMAC == None:
                        self.SMAC = smac
                    else:
                        assert smac == self.SMAC, 'something is wrong with this pcap, you have multiple macs coming from the same source ip'
                    if self.DMAC == None:
                        self.DMAC = dmac
                    else:
                        assert dmac == self.DMAC, 'something is wrong with this pcap, you have multiple macs coming from the same dest ip'
                    dmac= self.pcapfile[p][Ether].dst
                    append = [p,fromSrc, ack,seq, rawlen]
                    if self.IterPackets.has_key(flow) == True:
                        self.IterPackets[flow].append(append)
                    else:
                        self.IterPackets[flow] = []
                        self.IterPackets[flow].append(append)
                    
                elif (self.pcapfile[p][IP].dst == self.ORIGINALSRC ) and ( self.pcapfile[p][IP].src == self.ORIGINALDST ):
                    #print 'match dip_sip'
                    flow = '%s_%s' %(str(self.pcapfile[p][TCP].dport),str(self.pcapfile[p][TCP].sport))         
                    #print 'flow %s' %flow
                    fromSrc = False
                    smac= self.pcapfile[p][Ether].dst
                    dmac= self.pcapfile[p][Ether].src
                    if self.SMAC == None:
                        self.SMAC = smac
                    else:
                        assert smac == self.SMAC, 'something is wrong with this pcap, you have multiple macs coming from the same source ip'
                    if self.DMAC == None:
                        self.DMAC = dmac
                    else:
                        assert dmac == self.DMAC, 'something is wrong with this pcap, you have multiple macs coming from the same dest ip'
                    ack= self.pcapfile[p][TCP].ack
                    seq= self.pcapfile[p][TCP].seq
                    append = [p,fromSrc, ack,seq, rawlen]
                    if self.IterPackets.has_key(flow) == True:
                        self.IterPackets[flow].append(append)
                    else:
                        self.IterPackets[flow] = []
                        self.IterPackets[flow].append(append)
                else:
                    #print 'no match'
                    flow = 'NA'
                    append = [p,False,None, None, None,None, 0]
                    self.IterPackets[flow].append(append)
            else:
                #print 'packet not a tcp flow'
                flow = 'NA'
                append = [p,False,None, None, None,None, 0]
                self.IterPackets[flow].append(append)
                #print 'appended non importent packet to iterpackets'
            #packetlistappend = [p,flow]
            self.PacketList.append([p,
                                   flow,
                                   ])

    def FindPacketWithRawData(self,
                              RawData):
        #print 'finding packet with raw data'
        PacketNumber = -1
        for i in range(len(self.pcapfile)):
            if self.pcapfile[i].haslayer(Raw):
                #if len(self.pcapfile[i][Raw].load) < 300:
                    #print 'matching %s with %s' %(RawData, self.pcapfile[i][Raw].load)
                if str(RawData) in str(self.pcapfile[i][Raw].load):
                    #print 'found packet at %i' %i
                    return i
        print 'could not find packet with raw data= %s' %RawData
        raise Exception('could not find packet with raw data= %s' %RawData)
        

    def Create_Inject_Packet(self,
                             srcport,
                       dstport,
                       afterpacket,
                       raw = None,
                       SIMPACK = True,
                       FromSRC = True,
                             ):
        packets = []
        assert afterpacket <= len(self.pcapfile)
        #serch flow for packet
        flow = '%s_%s' %(srcport,dstport)
        for p,f in self.PacketList:
            if str(f) == str(flow):
                lastreleventpacket = p
            if int(p) == int(afterpacket):
                break
        
        #print 'relpack', lastreleventpacket
        #now search
        found = 0
        for packet in self.IterPackets[flow]:
            if packet[0] == lastreleventpacket:
                fromSrc = packet[1]
                ACK = int(packet[2])
                SEQ = int(packet[3])
                RAWLEN = int(packet[4])
                found = 1
                #print 'last relpack', self.pcapfile[lastreleventpacket].show
                break
            
        assert found ==1, 'could not find the packet to inject after'
  
        #setupether
        hasRaw = False
        ether = Ether()
        ip = IP()
        tcp = TCP()
        if raw != None:
            #print 'insert packet has raw'
            hasRaw = True
            tcp.flags = 'PA'
            RAW = Raw()
            RAW.load = raw
            rawlen = len(raw)
        else:
            tcp.flags = 'A'
            rawlen = 0

        
        if FromSRC == True:
            ether.dst = self.DMAC
            ether.src = self.SMAC
            ip.src = self.ORIGINALSRC
            ip.dst = self.ORIGINALDST
            tcp.sport = srcport
            tcp.dport = dstport
        else:
            ether.dst = self.SMAC
            ether.src = self.DMAC
            ip.dst = self.ORIGINALSRC
            ip.src = self.ORIGINALDST
            tcp.dport = srcport
            tcp.sport = dstport
        #print 'matching',FromSRC,fromSrc
        if FromSRC == fromSrc:
            tcp.ack = ACK
            tcp.seq = SEQ + RAWLEN
        else:
            tcp.ack = SEQ + RAWLEN
            tcp.seq = ACK

        if hasRaw == True:
            packet = ether/ip/tcp/RAW
            packet[IP].len = len(packet[IP])
            del packet[IP].chksum
            del packet[TCP].chksum
        else:
            packet = ether/ip/tcp
            packet[IP].len = len(packet[IP])
            del packet[IP].chksum
            del packet[TCP].chksum
        
        
        #print 'insert packet', packet.show
        packets.append(packet)

        try:
            LENGTH =len(packet[Raw])
        except IndexError:
            LENGTH = 0

        if SIMPACK == True:
            #swap and acknowledge
            #print 'insert an ackknowledge packet'
            sport = tcp.sport
            dport = tcp.dport
            sip = ip.src
            dip = ip.dst
            smac = ether.src
            dmac = ether.dst
            tcp.sport = dport
            tcp.dport = sport
            ip.dst = sip
            ip.src = dip
            ether.src = dmac
            ether.dst = smac
            SEQ = tcp.seq
            ACK = tcp.ack
            tcp.seq = ACK
            tcp.ack = SEQ + LENGTH
            tcp.flags = 'A'
            packet = ether/ip/tcp
            packet[IP].len = len(packet[IP])
            del packet[IP].chksum
            del packet[TCP].chksum
            #print 'insert packet', packet.show
            packets.append(packet)

        return packets, LENGTH
    
          
        
    def InjectPacket(self,
                     srcport,
                       dstport,
                       afterpacket = None,
                       raw = None,
                       SIMPACK = True,
                       FromSRC = True,
                    outputfile=None):
        flow = '%s_%s' %(srcport,dstport)
        #print 'injecting packet with raw date %s after packet %i' %(raw.strip('\n').strip('\r'),afterpacket)
        SENDPACKETS = []
        
        assert type(afterpacket) == int
        assert afterpacket <= len(self.pcapfile)
        #if port == None:
            #pack first flow

        #send packets normaly if before insersion
        for i in range(afterpacket):
            #p = self.PacketList[i][0]
            SENDPACKETS.append(self.pcapfile[i])
            #sendp(self.pcapfile[p], iface = self.INTF)

        #nowsend inserted packet
        Packets, OFFSET = self.Create_Inject_Packet(srcport,
                                                    dstport,
                                                    afterpacket,
                                                    raw = raw,
                                                    SIMPACK = SIMPACK,
                                                    FromSRC = FromSRC,
                                                    )
        SENDPACKETS += Packets

        #now send the rest of the packets
        for i in range(afterpacket,len(self.pcapfile)):
            packet = self.pcapfile[i]
            if packet.haslayer(TCP) == 1:
                if self.pcapfile[i][TCP].sport == srcport and self.pcapfile[i][TCP].dport == dstport:
                    packet[TCP].seq += OFFSET
                elif self.pcapfile[i][TCP].sport == dstport and self.pcapfile[i][TCP].dport == srcport:
                    packet[TCP].ack += OFFSET
                #else:
                    #print 'flow %s_%s, not match %s' %(self.pcapfile[i][TCP].sport,self.pcapfile[i][TCP].dport,flow)

            #sendp(packet)    
            SENDPACKETS.append(packet)

        #now send all packets
        #return SENDPACKETS
        try:
            #print 'original number of packets', len(self.pcapfile)
            sendp(SENDPACKETS,iface=self.INT)
        except Exception as error:
            print 'unable to send pcap with error:', error
            
        #write to pcap file
        if outputfile != None and outputfile != 'None':
            #write to pcap
            try:
                 wrpcap(outputfile,SENDPACKETS)
            except Exception as error:
                print 'unable to send pcap with error:', error
  

def main(argv):
    srcport = None
    dstport = None
    afterpacket = None
    raw = None
    #raw = 'ABOR\r\n'
    RawSearch = None
    SIMPACK = True
    FromSRC = True
    PCAPPATH = None
    ORIGINALSRC = None
    ORIGINALDST = None
    OUTPUT = None
    Before = False
    try:
        opts, args = getopt.getopt(argv, 'i:o:I:s:S:d:D:R:r:p:faB"',['input=',
                                                                    'output=',
                                                                    'interface=',
                                                                    'src-ip=',
                                                                    'dsrc-port=',
                                                                    'dst-ip=',
                                                                    'ddst-port=',
                                                                    'RawSearch=',
                                                                    'raw=',
                                                                    'afterpacket=',
                                                                     'fromsrc',
                                                                     'simpack',
                                                                     'Before',
                                                                    ]
                                   )
        
    except getopt.GetoptError:
        print 'failed to run error'
    for opt, arg in opts:
        
        if opt == '-f':
            FromSRC = False
        if opt == '-a':
            SIMPACK = False
        if opt == '-i':
            PCAPPATH = arg.strip('\n')
        if opt == '-o':
            outfile = arg.strip('\n')
        if opt == '-I':
            INT = arg.strip('\n')
        if opt == '-s':
            ORIGINALSRC = arg.strip('\n')
        if opt == '-d':
            ORIGINALDST = arg.strip('\n')
        if opt == '-S':
            srcport = int(arg.strip('\n'))
        if opt == '-D':
            dstport = int(arg.strip('\n'))
        if opt == '-R':
            RawSearch = arg.strip('\n')
        if opt == '-r':
            raw = arg.strip('\n')
        if opt == '-p':
            afterpacket = int(arg.strip('\n'))
        if opt == '-B':
            Before = True
        
    Insert(Before = Before,
           RawSearch = RawSearch,
           raw = raw,
           srcport = srcport,
         dstport = dstport,
         afterpacket = afterpacket,
         SIMPACK = SIMPACK,
         FromSRC = FromSRC,
         outputfile=outputfile,
         PCAPPATH= PCAPPATH,
         ORIGINALSRC=ORIGINALSRC,
         ORIGINALDST=ORIGINALDST,
         INT = INT)

def Insert(Before = True,
         RawSearch = '226 Transfer complete.',
         raw = 'ABOR\r\n',
         srcport = 39519,
         dstport = 21,
         afterpacket = None,
         SIMPACK = False,
         FromSRC = True,
         outputfile='Testinsert.pcap',
         PCAPPATH= '../../../FTPPCAPS/TESTCAPS/FTP_GET.pcap',
         ORIGINALSRC='192.168.122.1',
         ORIGINALDST='192.168.122.4',
         INT = 'wlan0'):
    
    #print 'attempting to inject packet into %s' %PCAPPATH
    #print 'pcap must have flow with SIP %s and DIP %s' %(ORIGINALSRC, ORIGINALDST)
    self = Scapy_PacketInjector(PCAPPATH,
                                ORIGINALSRC,
                                ORIGINALDST)
    self.INT = INT
    
    if afterpacket == None:
        afterpacket = random.randint(1,len(self.pcapfile))
    else:
        assert type(afterpacket) == int
        assert afterpacket <= len(self.pcapfile)
    #now get the right inject packet
    if RawSearch != None:
        try:
            afterpacket = self.FindPacketWithRawData(RawSearch)
            if Before == True:
                afterpacket -= 1
                
        except Exception as error:
            if 'could not find packet with raw data' in error:
                print 'could not find RawSearch'
                if afterpacket == None:
                    print 'will inject this packet randomly into stream'
                else:
                    if Before == True:
                        afterpacket -= 1

    return self.InjectPacket(srcport,
                      dstport,
                      afterpacket = afterpacket,
                      raw = raw,
                      SIMPACK = SIMPACK,
                      FromSRC = FromSRC,
                      outputfile=outputfile)
    

if __name__ == '__main__':
    #p = Insert()
    main(sys.argv[1:])

'''
EXAMPLE USE
python Scapy_PacketInjector.py -i "FTP.pcap" -o "FTPOUT.pcap" -I veth1 -s <original_src.ip> -S <flowchange_src.dataport> -d <original_dst.ip> -D <flowchange_dst.dataport> -R <rawsearch>  -r <insertpackerawdata> -B(optional to insert before the raw serch packet, otherwise it is after)
'''
##test mnt
#umount /mnt/tsn
#mount -t tmpfs -o size=4g none /mnt/tsn
#mount -t tmpfs -o size=4g tmpfs /mnt/tsn
