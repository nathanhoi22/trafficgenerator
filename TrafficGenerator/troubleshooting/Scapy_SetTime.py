

#! /usr/bin/env python
from scapy.all import *
import os
import sys, getopt
from time import sleep, time, gmtime


class ReplayClass():
    def __init__(self,TimeOffset):
        self.TimeOffset = TimeOffset
    def ReadPcap(self,
                 pcapfile):
        assert os.path.exists(pcapfile),'%s does not exist' %pcapfile
        return rdpcap(pcapfile)
    def editpackets(self,
                    RPCAP,  #this is a Parse_Pcap object
                    ):
        #first things first, set timestamps
        if self.TimeOffset != None:
            if 'now' in str(self.TimeOffset).lower():
                print 'now'
                self.TimeOffset = int(time()) - int(RPCAP[0].time)
            else:
                print self.TimeOffset
                
            self.TimeOffset = int(self.TimeOffset)
            print 'increasing time of packets by %i' %self.TimeOffset
            packetstarttime= '%i-%i-%i %i:%i:%i' %(gmtime(RPCAP[0].time+self.TimeOffset).tm_year,
                                                   gmtime(RPCAP[0].time+self.TimeOffset).tm_mon,
                                                   gmtime(RPCAP[0].time+self.TimeOffset).tm_mday,
                                                   gmtime(RPCAP[0].time+self.TimeOffset).tm_hour,
                                                   gmtime(RPCAP[0].time+self.TimeOffset).tm_min,
                                                   gmtime(RPCAP[0].time+self.TimeOffset).tm_sec)
            print 'expect packet start time to be', packetstarttime

            for packet in RPCAP:
                packet.time += self.TimeOffset

            
                    
   


    def replaypackets (self,
                       RPCAP, #this is a Parse_Pcap object
                       ):
        
        #sendp(RPCAP,iface=self.INT)
        
        for PACKET in RPCAP:
            sendp(fragment(PACKET, fragsize=1480),iface=self.INT, verbose = 0)
       

        if self.outfile != None and self.outfile != 'None':
            #write to pcap

            print 'writing to %s' %self.outfile
            wrpcap(self.outfile,RPCAP)
                                        

  

def main(argv):
    infile = None #pcap to read
    outfile = None #pcap to write
    TimeOffset = 'Now'
    INTERFACE = None
    showhelp = False
    try:
        opts, args = getopt.getopt(argv, 't:i:o:I:h',['TimeOffset=',
                                                     'input=',
                                                     'output=',
                                                     'interface=',
                                                     ],
                                   )
    except getopt.GetoptError:
        print 'failed to run error'
    
    TimeOffset = None
    for opt, arg in opts:
        if opt == '-t':
            if 'now' in str(arg.strip('\n')).lower():
                TimeOffset = 'now'
            else:
                TimeOffset = int(arg.strip('\n'))
        if opt == '-i':
            infile = arg.strip('\n')
        if opt == '-o':
            outfile = arg.strip('\n')
        if opt == '-I':
            INTERFACE = arg.strip('\n')
        if opt == '-h':
            showhelp = True
    if showhelp == True:
        print '''
        Scapy_SetTime.py is solely designed to set the time of a pcap 
            ARGS
                
                -i input pcap  --> pcap file to edit the time of
                -o output pcap  --> pcap file to write to
                -t  time offset --> increase time of pcap by number of seconds(default sets to present time)
                -I interface --> interface to replay on  

            Examples:
                #replay pcap in present time on interface eth0
                python Scapy_SetTime.py -i mypcap.pcap -I eth0 

                #add 300 seconds to pcap, replay, and save new pcap file
                python Scapy_SetTime.py -i mypcap.pcap -I eth0 -o newpcap.pcap -t 300
            '''
    else:

    
        assert INTERFACE != None, 'must define replay interface with -I'
        assert infile != None, 'must define infile with -i'
        assert TimeOffset != None, 'must define time offset with -t'
        Replay = ReplayClass(TimeOffset)
        RDPCAP = Replay.ReadPcap(infile)
        Replay.outfile = outfile
        Replay.INT = INTERFACE
        Replay.editpackets(RDPCAP)
        Replay.replaypackets(RDPCAP)


if __name__ == '__main__':
    main(sys.argv[1:])

