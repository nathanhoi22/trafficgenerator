from scapy.all import *
from subprocess import Popen, PIPE
import os
import sys
from time import sleep

def copypcapfile(pcapfile,copyas):
    assert os.path.exists(pcapfile), '%s does not exist' %pcapfile
    packets = rdpcap(pcapfile)
    
    f = open(copyas, 'w')
    sleep(.01)
    sys.displayhook(f)
    sys.stdout = f
    for packet in range(len(packets)):
        print 'packet number', packet
        packets[packet].show()
        

    f.close()


##only do one at a time
#copypcapfile('../badmulit.pcap', 'badmulti.txt')
copypcapfile('../goodmulit.pcap', 'goodmulti.txt')
